const fs = require('fs');

function getCurrentLogs() {
  const currentLogs = fs.readFileSync('logs.json', 'utf8');
  return currentLogs ? JSON.parse(currentLogs) : { logs: []};
}

function updateLogs(currentLog, message) {
  const log = {
      message: message,
      time: Date.now()
  }
  currentLog.logs.push(log);
}

function writeLogs(currentLogs) {
  fs.writeFile('logs.json', JSON.stringify(currentLogs), 'utf8', (err) => {
      if (err) {
          console.log(err);
          return;
      }
  });
}

module.exports = {
  getCurrentLogs,
  updateLogs,
  writeLogs
}