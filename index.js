const http = require('http');
const fs = require('fs');
const url = require('url');
const controllers = require('./Controllers/LogController');

module.exports = () => {
    let currentLogs = controllers.getCurrentLogs();
    http.createServer(function(request, response) {
        const { pathname, query } = url.parse(request.url, true);

        if (request.method === 'POST') {
            fs.writeFile(`./file/${query.filename}`, query.content, 'utf8', (err) => {
                if (err) {
                    response.writeHead(400, { 'Content-type': 'text/html' });
                    response.end(`There was an error creating file ${query.filename}`);
                    return;
                }
                controllers.updateLogs(currentLogs, `New file with name '${query.filename}' saved`);
                controllers.writeLogs(currentLogs);
    
                response.writeHead(200, {'Content-type': 'text/html'});
                response.end();
            });
        }
        if (request.method === 'GET') {
            const reqPath = pathname.split('/')[1];
            if (reqPath === 'file') {
                fs.readFile(`.${pathname}`, (err, data) => {
    
                    controllers.updateLogs(currentLogs, `GET request of file ${pathname.split('/')[2]}`);
                    controllers.writeLogs(currentLogs);
    
                    if (err) {
                        response.writeHead(400, { 'Content-type': 'text/html' });
                        response.end(`File "${pathname.split('/')[2]}" was not found`);
                        return;
                    }
                    response.writeHead(200, {'Content-type': 'text/html'});
                    response.end(data);
                })
            }
            if (reqPath === 'logs') {
                response.writeHead(200, {'Content-type': 'application/json'});
                response.end(JSON.stringify(currentLogs));
            }
        }
    }).listen(process.env.PORT || 8080);
}